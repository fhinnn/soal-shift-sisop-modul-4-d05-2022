# Penyelesaian Soal Shift 4 Sistem Operasi

## Kelompok D05

| Nama | NRP |
| ------ | ------ |
| DHAFIN ALMAS NUSANTARA | 5025201064 |
| NEISA HIBATILLAH ALIF | 5025201170 |
| FEBERLIZER EDNAR WILLIAM GULTOM|05111940007004|


## ---SOAL 1---
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:

## Soal a
Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13

	Contoh : 
	“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”
## Penjelasan:

Untuk klasifikasi folder mau di encode atau di decode saya menggunakan fungsi is Animeku yang mengembalikan nilai boolean true atau false yang nantinya sistem akan tahu file akan diencode atau didecode nantinya.

Fungsi isAnimeku:
```
bool isAnimeku(const char *path) 
{
    for(int i=0;i<strlen(path)-8+1;i++)
        if(path[i] == 'A' && path[i+1] == 'n' && path[i+2] == 'i' && path[i+3] == 'm' && path[i+4] == 'e'
        && path[i+5] == 'k' && path[i+6] == 'u' && path[i+7] == '_') return 1;
    return 0;
}
```
Fungsi Decode dan Encode :
```
void encodeAtRot(char *s)
{
    for(int i=0;i<strlen(s);i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+13)%26)+'a';
}

void decodeAtRot(char *s)
{
    for(int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
        else if(s[i]>='a'&&s[i]<110) s[i] = ((s[i]-'a'-13)+26)+'a';
        else if(s[i]>=110&&s[i]<='z') s[i] = ((s[i]-'a'-13)%26)+'a';
}
```
## Soal b dan c
Semua direktori di-rename dengan awalan “ Animeku_ ”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.


## Penjelasan :

Fungsi fuse untuk xmp_rename akan dipanggil ketika terdapat file yang direname. Pada fungsi ini, kita bisa mengecek apakah direktori diubah menjadi direktori yang akan di-encode atau di-decode.

fpath  atau tpath akan di-encode ketika memiliki substring "Animeku_". Jika tidak, direktori tersebut akan di-decode.

fungsi fuse xmp_rename:
```
        if (isAnimeku(fpath) && !isAnimeku(tpath)) 
        {
            printf("[Mendekode %s.]\n", fpath);
            sistemLog(fpath, tpath, 2);
            int itung = decodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terdekode: %d]\n", itung);
        }
        else if (!isAnimeku(fpath) && isAnimeku(tpath)) 
        {
            printf("[Mengenkode %s.]\n", fpath);
            sistemLog(fpath, tpath, 1);
            int itung = encodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terenkode: %d]\n", itung);
```
## Soal d
Setiap data yang terencode akan masuk dalam file “Wibu.log” 

Contoh isi:
``` 
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba
```
## Penjelasan
Untuk menyelesaikan problem ini, kami membuat fungsi logRename yang terintegrasi dengan fungsi sistemLog yang dapat mencatat aktivitas rename dari suatu direktori. Fungsi ini nantinya akan dipanggil sebelum proses encode maupun decode dieksekusi.

Fungsi sistemLog:
```
void sistemLog(char *dir1, char *dir2, int tipe) 
{
    char buff[1024], cmd[32];
    if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logIngfo(cmd,buff);
    else{
        if(tipe == 3){ //mkdir
            strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logIngfo(cmd, buff);
        }else if(tipe == 4){ //rmdir
            strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }else if(tipe == 5){ //unlink
            strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
    }    
}
```
fungsi log rename :
```
void logRename(char *cmd, int tipe, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
    char logNya[1100];
    sprintf(logNya, "%s %s %s", cmd, tipe==1?"terenkripsi":"terdecode", des);
    FILE *out = fopen(fileLog, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
    return;
}
```
## Soal e
Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

## Penjelasan
saya menggunakan library dirent.h di fungsi decodeFolderRekursif maupun fungsi encodeFolderRekursif dengan nilai yang dikembalikan adalah total file yang berhasil di-encode ataupun di-decode.
Pada fungsi encodeFolderRekursif dan decodeFolderRekursif akan dilakukan proses scan file folder di dalamnya. Proses decode maupun encode akan dilanjutkan pada suatu folder yang berhasil ditemukan berdasarkan parameter basePath yang dimasukkan saat pemanggilan fungsi.

Fungsi encodeFolderRekursif:
```
int encodeFolderRekursif(char *basePath, int depth) 
{
    char path[1000]; 
    struct dirent *dp; 
    DIR *dir = opendir(basePath);
    if (!dir) return 0;
    int itung=0;
    while((dp=readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode)&&depth>0)
            itung += encodeFolderRekursif(path, depth - 1),
            encodeFolder(basePath, dp->d_name);
        else if(encodeFile(basePath, dp->d_name) == 0) itung++;
    }
    closedir(dir);
    return itung;
}
```
Fungsi decodeFolderRekursif:
```
int decodeFolderRekursif(char *basePath, int depth) 
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    if(!dir) return 0;
    int itung = 0;
    while((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode) && depth>0)
            itung += decodeFolderRekursif(path, depth - 1),
            decodeFolder(basePath, dp->d_name);
        else if(decodeFile(basePath, dp->d_name) == 0) itung++;
    }
    closedir(dir);
    return itung;
}
```

## ---SOAL 2---
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut:

## Soal a
Jika suatu direktori dibuat dengan awalan `IAN_[nama]`, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key `INNUGANTENG` (Case-sensitive, Vigenere).

Fungsi isIAN:
```
bool isIAN(const char *path) 
{
    for(int i=0;i<strlen(path)-4+1;i++)
        if(path[i] == 'I' && path[i+1] == 'A' && path[i+2] == 'N' && path[i+3] == '_') return 1;
    return 0;
}
```

Penjelasan:
- Jika fungsi isAnimeku bernilai true, yaitu ketika terdapat substring `IAN_`, maka path akan di-encode.
- Jika fungsi isAnimeku bernilai false, maka path akan di-decode.

Fungsi encodeVig untuk meng-encode Vigenere cipher:
```
void encodeVig(char *s) 
{
    char key[] = "INNUGANTENG";
    for (int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'+(key[i%((sizeof(key)-1))]-'A'))%26)+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+(key[i%((sizeof(key)-1))]-'A'))%26)+'a';
}
```

Fungsi decodeVig untuk meng-decode Vigenere cipher:
```
void encodeVig(char *s) 
{
    char key[] = "INNUGANTENG";
    for (int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'+(key[i%((sizeof(key)-1))]-'A'))%26)+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+(key[i%((sizeof(key)-1))]-'A'))%26)+'a';
}
```

## Soal b dan c
Jika suatu direktori di rename dengan `IAN_[nama]`, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.
Apabila nama direktori dihilangkan `IAN_`, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.

Fungsi xmp_rename:
```
...
    else if(isIAN(fpath) && !isIAN(tpath))
    {
        printf("[Mendekode %s.]\n", fpath);
        sistemLog(fpath, tpath, 2);
        int itung = decodeFolderRekursifIAN(fpath, 1000);
        printf("[Total file yang terdekode: %d]\n", itung);
    }
    else if(!isIAN(fpath) && isIAN(tpath))
    {
        printf("[Mengenkode %s.]\n", fpath);
        sistemLog(fpath, tpath, 1);
        int itung = encodeFolderRekursifIAN(fpath, 1000);
        printf("[Total file yang terenkode: %d]\n", itung);
    }
...
```

Penjelasan:
-
- Fungsi ini dipanggil ketika terdapat direktori atau file yang direname.
- String `fpath` digunakan untuk menyimpan nama direktori sebelum dan `tpath` untuk menyimpan nama direktori setelah di-rename.
- `fpath` atau `tpath` akan di-encode ketika memiliki substring `Animeku_`. Jika tidak, maka akan di-decode.

## Soal d dan e
Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada file system yang telah dibuat, ia membuat log system pada direktori `/home/[user]/hayolongapain_[kelompok].log`. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada file system.
Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall `rmdir` dan `unlink`. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut: 
`[Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]`

Fungsi logIngfo:
```
void logIngfo(char *cmd, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
    char logNya[1100];
    sprintf(logNya, "INFO::%s:%s::%s", waktu, cmd, des);
    FILE *out = fopen(fileLog, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
    return;
}
```

Fungsi logWarning:
```
void logWarning(char *cmd, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t); 
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt); 
    char logNya[1100];
    sprintf(logNya, "WARNING::%s:%s::%s", waktu, cmd, des); 
    FILE *out = fopen(fileLog, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
}
```

Fungsi sistemLog:
```
void sistemLog(char *dir1, char *dir2, int tipe) 
{
    char buff[1024], cmd[32];
    if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logIngfo(cmd,buff);
    else{
        // mkdir
        if(tipe == 3){
            strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logIngfo(cmd, buff);
        }
        // rmdir
        else if(tipe == 4){
            strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
        // unlink
        else if(tipe == 5){
            strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
    } 

}
```
Penjelasan:
- Fungsi sistemLog mencatat aktivitas rename suatu direktori dan dipanggil sebelum proses encode maupun decode.
- Fungsi sistemLog menerima parameter tipe, yaitu variable yang akan menentukan kategori dari sebuah system call. Tipe 3 termasuk INFO, sementara tipe 4 dan 5 termasuk warning (RMDIR atau UNLINK).


## ---SOAL 3---
Ishaq adalah seseorang yang terkenal di kalangan anak informatika seluruh indonesia. Ia memiliki teman yang bernama Innu dan Anya, lalu Ishaq bertemu dengan mereka dan melihat program yang mereka berdua kerjakan sehingga ia tidak mau kalah dengan Innu untuk membantu Anya dengan menambah fitur yang ada pada programnya dengan ketentuan:

## Soal a
Jika suatu direktori dibuat dengan awalan `nam_do-saq_`, maka direktori tersebut akan menjadi sebuah direktori spesial.

Fungsi isNamdosaq:
```
bool isNamdosaq(const char *path) 
{
    for(int i=strlen(path)-1;i>=11;i--)
        if (path[i-11] == 'n' && path[i-10] == 'a' && path[i-9] == 'm' && path[i-8] == '_' &&
            path[i-7] == 'd' && path[i-6] == 'o' && path[i-5] == '-' && path[i-4] == 's' &&
            path[i-3] == 'a' && path[i-2] == 'q' && path[i-1] == '_') return 1;
    return 0;
}
```

## Soal b dan c
Jika suatu direktori di-rename dengan memberi awalan `nam_do-saq_`, maka direktori tersebut akan menjadi sebuah direktori spesial.

Apabila direktori yang terenkripsi di-rename dengan menghapus `nam_do-saq_` pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.

Fungsi xmp_rename:
```
...
    // rename Namdosaq to Animeku
    else if (isNamdosaq(fpath) && isAnimeku(tpath))
    {
        encryptSpecial(fpath);        
    }
    // rename Namdosaq to IAN
    else if (isNamdosaq(fpath) && isIAN(tpath))
    {
        encryptSpecial(fpath);
    }
    // rename Animeku to Namdosaq
    else if (isAnimeku(fpath) && isNamdosaq(tpath))    
    {
        decryptSpecial(fpath);
        sistemLog(fpath, tpath, 2);
    }
    // rename IAN to Namdosaq
    else if (isIAN(fpath) && isNamdosaq(tpath))
    {
        decryptSpecial(fpath);
        sistemLog(fpath, tpath, 2);
    }
...
```

## Soal d dan e
Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori `Animeku_` maupun `IAN_` namun masing masing aturan mereka tetap berjalan padadirektori di dalamnya (sifat recursive `Animeku_` dan `IAN_` tetap berjalan pada subdirektori).

Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.
Contoh : jika pada direktori asli namanya adalah `isHaQ_KEreN.txt` maka pada fuse akan menjadi `ISHAQ_KEREN.txt.1670`. 1670 berasal dari biner 11010000110.

Fungsi getBiner:
```
void getBiner(char *path, char *biner, char *uppercase){
	int idFirst = 0;
    for(int i=0; i<strlen(path); i++){
		if (path[i] == '/'){ 
            idFirst = i + 1;
        }
	}
    
    int idEnd = strlen(path);
    for(int i=strlen(path)-1; i>=0; i--){
		if (path[i] == '.'){ 
            idEnd = 1;
        }
	}

	int i;
	for(i=idFirst; i<idEnd; i++){
		if(isupper(path[i])){
			biner[i] = '0';
            uppercase[i] = path[i];
		}
		else{
			biner[i] = '1';
            uppercase[i] = path[i] - 32;
		}
	}
	biner[idEnd] = '\0';
	uppercase[i] = '\0';
}
```

Fungsi binerToDecimal:
```
int binerToDecimal(char *biner){
	int temp = 1, result = 0;
	for(int i=strlen(biner)-1; i>=0; i--){
        if(biner[i] == '1'){
            result += temp; 
            temp *= 2;
        }
    }
	return result;
}
```

Fungsi getDecimal:
```
void getDecimal(char *path, char *biner, char *normalcase){
	int idFirst = 0;
    for(int i=0; i<strlen(path); i++){
		if (path[i] == '/'){ 
            idFirst = i + 1;
        }
	}
    
    int idEnd = strlen(path);
    for(int i=strlen(path)-1; i>=0; i--){
		if (path[i] == '.'){ 
            idEnd = 1;
        }
	}
	int i;
	
	for(i=idFirst; i<idEnd; i++){
		if(biner[i-idFirst] == '1') normalcase[i-idFirst] = path[i] - 32;
		else normalcase[i-idFirst] = path[i];
	}
	
	for(; i<strlen(path); i++){
		normalcase[i-idFirst] = path[i];
	}
	normalcase[i-idFirst] = '\0';
}
```

Fungsi decimalToBiner:
```
void decimalToBiner(int decimal, char *biner, int len){
	int idx = 0;
	while(decimal){
		if(decimal & 1){
            biner[idx] = '1';
        }
		else {
            biner[idx] = '0';
            idx++;
        }
		decimal /= 2;
	}
	while(idx < len){
		biner[idx] = '0'; idx++;
	}
	biner[idx] = '\0';
	
	for(int i=0; i<idx/2; i++){
		char temp = biner[i];
        biner[i] = biner[idx-1-i];
        biner[idx-1-i] = temp;
	}
}
```

Fungsi convertDecimal:
```
int convertDecimal(char *ext){
	int decimal = 0, temp = 1;
	for(int i=strlen(ext)-1; i>=0; i--){
        decimal += (ext[i]-'0')*temp;
        temp *= 10;
    }
	return decimal;
}
```

Fungsi encryptSpecial:
```
void encryptSpecial(char *filepath){
	chdir(filepath);
	DIR *dir = opendir(".");
	struct dirent *dp;
	struct stat path_stat;
	if(dir == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char newFilePath[1000];
	
    while ((dp = readdir(dir)) != NULL){
		if (stat(dp->d_name, &path_stat) < 0);
		else if (S_ISDIR(path_stat.st_mode)){
			if (strcmp(dp->d_name,".") == 0 || strcmp(dp->d_name,"..") == 0){
                continue;
                printf(dirPath,"%s/%s",filepath, dp->d_name);
                encryptSpecial(dirPath);
            }
		}
        else{
			sprintf(filePath,"%s/%s",filepath, dp->d_name);
			char biner[1000], uppercase[1000]; 
            getBiner(dp->d_name, biner, uppercase);
			int decimal = binerToDecimal(biner);
			printf(newFilePath,"%s/%s.%d",filepath,uppercase,decimal); 
            rename(filePath, newFilePath);
		}
	}
    closedir(dir);
}
```

Fungsi decryptSpecial:
```
void decryptSpecial(char *filepath){
	chdir(filepath);
	DIR *dir = opendir(".");
	struct dirent *dp;
	struct stat stat_path;
	if(dir == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char newFilePath[1000];
	
    while ((dp = readdir(dir)) != NULL){
		if (stat(dp->d_name, &stat_path) < 0);
		else if (S_ISDIR(stat_path.st_mode)){
			if (strcmp(dp->d_name,".") == 0 || strcmp(dp->d_name,"..") == 0) continue;
            sprintf(dirPath,"%s/%s",filepath, dp->d_name);
            decryptSpecial(dirPath);
		}
		else{
			sprintf(filePath,"%s/%s",filepath, dp->d_name);
			char fname[1000], bin[1000], normalcase[1000], clearPath[1000];
			
			strcpy(fname, dp->d_name);
			char *ext = strrchr(fname, '.');
			int dec = convertDecimal(ext+1);
			for(int i=0; i<strlen(fname)-strlen(ext); i++) clearPath[i] = fname[i];
			
			char *ext2 = strrchr(clearPath, '.');
			decimalToBiner(dec, bin, strlen(clearPath)-strlen(ext2));
            getDecimal(clearPath, bin, normalcase);
            printf(newFilePath,"%s/%s",filepath,normalcase);
            rename(filePath, newFilePath);
		}
	}
    closedir(dir);
}
```

Kendala yang dialami:
- Masih kurang memahami fuse.
- Soal rumit.
- Waktu pengerjaan praktikum yang dilakukan saat libur lebaran.
